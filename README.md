
#Calculator

##Summary

This is just a basic calculator implementation I made in 2003, which nevertheless still works in modern browsers.

As the code is ancient and outdated, I'm releasing it into the public domain.

##Demo

For a live demo, go to [http://jslegers.github.com/calculator/](http://jslegers.github.com/calculator/).